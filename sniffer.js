// To debug or not to debug
const DEBUG = (process.argv.lastIndexOf('-v') !== -1) ? true: false;
DEBUG && console.log('GO84 Sniffer');

const child_process = require('child_process');
const os            = require('os');
const StringDecoder = require('string_decoder').StringDecoder;
const decoder       = new StringDecoder('utf8');
const io            = require('socket.io-client');

// Log hostname and IP address
// const hostname  = os.hostname();
// const ipAddress = os.networkInterfaces().wlan0[0].address;
// DEBUG && console.log(hostname + ' on ' + ipAddress);

if (process.argv.lastIndexOf('-h') !== -1) {
  console.log(
    `Usage:
    -v  Verbose
    -n  Don't create monitoring interface`
  );
}

// Create monitoring interface
if (process.argv.lastIndexOf('-n') === -1) {
  const mon_init = child_process.execFile('bash',['init_monitoring.sh'], (err, stdout, stderr) => {
    if (err) throw err;
    DEBUG && console.log('Monitoring interface mon0 created');
  } );
}

// Connect to Socket.io server
const serverUrl = 'http://192.168.101.125:3000';
const conn = io.connect(serverUrl);

conn.on('connect', () => {
  DEBUG && console.log('Socket connected');
});

conn.on('connect_timeout', () => {
  DEBUG && console.log('Socket connect timeout');
});

conn.on('connect_error', (error) => {
  DEBUG && console.log('Socket connect error: ', error);
});

conn.on('error', (error) => {
  DEBUG && console.log('Socket error: ', error);
});

// Start sniffing child process
const tshark = child_process.spawn(
  'stdbuf',
  [
    '-oL',
    'tshark',
    '-i',
    'mon0',
    '-f',
    'broadcast',
    '-Y',
    'wlan.fc.type==0\&\&wlan.fc.subtype==4',
    '-T',
    'fields',
    '-E',
    'separator=@',
    '-E',
    'header=y',
    '-e',
    'frame.time_epoch',
    '-e',
    'wlan.sa',
    '-e',
    'radiotap.dbm_antsignal'
  ]
);

tshark.stdout.on('data', (data) => {
  var rawData = decoder.write(data).replace("\n", '').split('@');
  var measurement = {
    timestamp: rawData[0],
    mac: rawData[1],
    rssi0: rawData[2].substr(0, 3),
    rssi1: rawData[2].substr(4, 3),
    sniffer: os.hostname()
  };

  //DEBUG && console.log('Measurement: ', measurement);

  conn.emit('sendData', JSON.stringify(measurement));
});

tshark.stderr.on('data', (data) => {
  DEBUG && console.log('ERROR: ', decoder.write(data));
});

tshark.on('close', (code) => {
  DEBUG && console.log('tshark exited with code ', code);
});

// Log sniffer ip
setTimeout(function () {
  var myIp = {
    sniffer: os.hostname(),
    ip: os.networkInterfaces().wlan0[0].address
  };

  conn.emit('pingEvent', JSON.stringify(myIp), function (resp, data) {});
}, 5000);
